# Tegra Timings Generator
# Copyright (C) 2020 Ion Agorria
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import argparse
import errno
import importlib
import os

# indexed keys for Tegra 7000f000 memory controller config
MC_CFG_KEYS = {
    "T2": None,  # Seems like Tegra 2 doesn't have MC keys
    "T3": [
        "MC_EMEM_ARB_CFG",
        "MC_EMEM_ARB_OUTSTANDING_REQ",
        "MC_EMEM_ARB_TIMING_RCD",
        "MC_EMEM_ARB_TIMING_RP",
        "MC_EMEM_ARB_TIMING_RC",
        "MC_EMEM_ARB_TIMING_RAS",
        "MC_EMEM_ARB_TIMING_FAW",
        "MC_EMEM_ARB_TIMING_RRD",
        "MC_EMEM_ARB_TIMING_RAP2PRE",
        "MC_EMEM_ARB_TIMING_WAP2PRE",
        "MC_EMEM_ARB_TIMING_R2R",
        "MC_EMEM_ARB_TIMING_W2W",
        "MC_EMEM_ARB_TIMING_R2W",
        "MC_EMEM_ARB_TIMING_W2R",
        "MC_EMEM_ARB_DA_TURNS",
        "MC_EMEM_ARB_DA_COVERS",
        "MC_EMEM_ARB_MISC0",
        "MC_EMEM_ARB_RING1_THROTTLE"
    ]
}

# indexed keys for Tegra 7000f400 memory controller config
EMC_CFG_KEYS = {
    "T2": [
        "RC",
        "RFC",
        "RAS",
        "RP",
        "R2W",
        "W2R",
        "R2P",
        "W2P",
        "RD_RCD",
        "WR_RCD",
        "RRD",
        "REXT",
        "WDV",
        "QUSE",
        "QRST",
        "QSAFE",
        "RDV",
        "REFRESH",
        "BURST_REFRESH_NUM",
        "PDEX2WR",
        "PDEX2RD",
        "PCHG2PDEN",
        "ACT2PDEN",
        "AR2PDEN",
        "RW2PDEN",
        "TXSR",
        "TCKE",
        "TFAW",
        "TRPAB",
        "TCLKSTABLE",
        "TCLKSTOP",
        "TREFBW",
        "QUSE_EXTRA",
        "FBIO_CFG6",
        "ODT_WRITE",
        "ODT_READ",
        "FBIO_CFG5",
        "CFG_DIG_DLL",
        "DLL_XFORM_DQS",
        "DLL_XFORM_QUSE",
        "ZCAL_REF_CNT",
        "ZCAL_WAIT_CNT",
        "AUTO_CAL_INTERVAL",
        "CFG_CLKTRIM_0",
        "CFG_CLKTRIM_1",
        "CFG_CLKTRIM_2",
    ],
    "T3": [
        "EMC_RC",
        "EMC_RFC",
        "EMC_RAS",
        "EMC_RP",
        "EMC_R2W",
        "EMC_W2R",
        "EMC_R2P",
        "EMC_W2P",
        "EMC_RD_RCD",
        "EMC_WR_RCD",
        "EMC_RRD",
        "EMC_REXT",
        "EMC_WEXT",
        "EMC_WDV",
        "EMC_QUSE",
        "EMC_QRST",
        "EMC_QSAFE",
        "EMC_RDV",
        "EMC_REFRESH",
        "EMC_BURST_REFRESH_NUM",
        "EMC_PRE_REFRESH_REQ_CNT",
        "EMC_PDEX2WR",
        "EMC_PDEX2RD",
        "EMC_PCHG2PDEN",
        "EMC_ACT2PDEN",
        "EMC_AR2PDEN",
        "EMC_RW2PDEN",
        "EMC_TXSR",
        "EMC_TXSRDLL",
        "EMC_TCKE",
        "EMC_TFAW",
        "EMC_TRPAB",
        "EMC_TCLKSTABLE",
        "EMC_TCLKSTOP",
        "EMC_TREFBW",
        "EMC_QUSE_EXTRA",
        "EMC_FBIO_CFG6",
        "EMC_ODT_WRITE",
        "EMC_ODT_READ",
        "EMC_FBIO_CFG5",
        "EMC_CFG_DIG_DLL",
        "EMC_CFG_DIG_DLL_PERIOD",
        "EMC_DLL_XFORM_DQS0",
        "EMC_DLL_XFORM_DQS1",
        "EMC_DLL_XFORM_DQS2",
        "EMC_DLL_XFORM_DQS3",
        "EMC_DLL_XFORM_DQS4",
        "EMC_DLL_XFORM_DQS5",
        "EMC_DLL_XFORM_DQS6",
        "EMC_DLL_XFORM_DQS7",
        "EMC_DLL_XFORM_QUSE0",
        "EMC_DLL_XFORM_QUSE1",
        "EMC_DLL_XFORM_QUSE2",
        "EMC_DLL_XFORM_QUSE3",
        "EMC_DLL_XFORM_QUSE4",
        "EMC_DLL_XFORM_QUSE5",
        "EMC_DLL_XFORM_QUSE6",
        "EMC_DLL_XFORM_QUSE7",
        "EMC_DLI_TRIM_TXDQS0",
        "EMC_DLI_TRIM_TXDQS1",
        "EMC_DLI_TRIM_TXDQS2",
        "EMC_DLI_TRIM_TXDQS3",
        "EMC_DLI_TRIM_TXDQS4",
        "EMC_DLI_TRIM_TXDQS5",
        "EMC_DLI_TRIM_TXDQS6",
        "EMC_DLI_TRIM_TXDQS7",
        "EMC_DLL_XFORM_DQ0",
        "EMC_DLL_XFORM_DQ1",
        "EMC_DLL_XFORM_DQ2",
        "EMC_DLL_XFORM_DQ3",
        "EMC_XM2CMDPADCTRL",
        "EMC_XM2DQSPADCTRL2",
        "EMC_XM2DQPADCTRL2",
        "EMC_XM2CLKPADCTRL",
        "EMC_XM2COMPPADCTRL",
        "EMC_XM2VTTGENPADCTRL",
        "EMC_XM2VTTGENPADCTRL2",
        "EMC_XM2QUSEPADCTRL",
        "EMC_XM2DQSPADCTRL3",
        "EMC_CTT_TERM_CTRL",
        "EMC_ZCAL_INTERVAL",
        "EMC_ZCAL_WAIT_CNT",
        "EMC_MRS_WAIT_CNT",
        "EMC_AUTO_CAL_CONFIG",
        "EMC_CTT",
        "EMC_CTT_DURATION",
        "EMC_DYN_SELF_REF_CONTROL",
        "EMC_FBIO_SPARE",
        "EMC_CFG_RSV",
    ]
}

# Per timings config
TIMINGS_CFG_KEYS = {
    "T2": {
        "SDRAM frequency":                      "clock-frequency",
    },
    "T3": {
        "Rev 3.1":                              None,  # Not used in DT
        "Rev 3.2":                              None,  # Not used in DT
        "SDRAM frequency":                      "clock-frequency",
        "EMC_AUTO_CAL_INTERVAL":                "nvidia,emc-auto-cal-interval",
        "Mode Register 1":                      "nvidia,emc-mode-1",
        "Mode Register 2":                      "nvidia,emc-mode-2",
        "Mode Register 0":                      "nvidia,emc-mode-reset",
        "EMC_ZCAL_WAIT_CNT after clock change": "nvidia,emc-zcal-cnt-long",
        "EMC_CFG.DYN_SELF_REF":                 "nvidia,emc-cfg-dyn-self-ref",
        "EMC_CFG.PERIODIC_QRST":                "nvidia,emc-cfg-periodic-qrst",
    }
}

TIMINGS_CFG_KEYS_FLAGS = [
    "nvidia,emc-cfg-periodic-qrst",
    "nvidia,emc-cfg-dyn-self-ref"
]


# Attempts to match comment with keys
def find_key(comment, keys):
    if keys is not None:
        for key in keys:
            if comment == key:
                return key
    return None


# Attempts to parse struct to generate a more handleable content
def parse_ram_struct(struct, soc):
    timings = []
    # Depth tell us where we are, once we come back to root we shouldn't be opening again
    # 0 = outside root
    # 1 = contains timing configs
    # 2 = contains per timings config keys
    # 3 = contains MC / EMC configuration
    depth = 0
    first_depth = True
    current_timing_config = {}
    for line_index, line in enumerate(struct.splitlines()):
        line = line.strip()
        if line == "":
            continue

        # Check if is a value with comment
        if "," in line and "/*" in line and "*/" in line:
            # We need to swap content and comment
            line_split = line.split(",")
            content = line_split[0].strip()
            comment = ",".join(line_split[1:])

            # Remove any struct member part
            content_split = content.split("=")
            content = content_split[-1].strip()

            # Attempt to extract key from comment
            if "/*" not in comment or "*/" not in comment:
                raise Exception("Uh, comment is not in comment part? %s:%s" % (line_index, line))
            comment = comment.replace("/*", "").replace("*/", "").strip()

            # Attempt to place this key
            found_key = None
            if depth == 2:
                found_key = find_key(comment, TIMINGS_CFG_KEYS[soc].keys())
                if found_key is not None:
                    if found_key in current_timing_config:
                        raise Exception("Timing key already added at depth %s %s:%s" % (depth, line_index, line))
                    current_timing_config[found_key] = content

            if depth == 3:
                found_key = find_key(comment, MC_CFG_KEYS[soc])
                if found_key is not None:
                    if "mc" not in current_timing_config:
                        current_timing_config["mc"] = {}
                    if found_key in current_timing_config["mc"]:
                        raise Exception("MC key already added at depth %s %s:%s" % (depth, line_index, line))
                    current_timing_config["mc"][found_key] = content
                else:
                    found_key = find_key(comment, EMC_CFG_KEYS[soc])
                    if found_key is not None:
                        if "emc" not in current_timing_config:
                            current_timing_config["emc"] = {}
                        if found_key in current_timing_config["emc"]:
                            raise Exception("EMC Key already added at depth %s %s:%s" % (depth, line_index, line))
                        current_timing_config["emc"][found_key] = content

            if found_key is None:
                raise Exception("Unknown key at depth %s %s:%s" % (depth, line_index, line))
        elif line[-1] == "{":
            if depth <= 0 and not first_depth:
                raise Exception("Attempted to open a closed struct? %s:%s" % (line_index, line))
            depth += 1
            if depth > 3:
                raise Exception("Attempted to go too depth? %s:%s" % (line_index, line))
        elif line[0] == "}":
            depth -= 1
            if depth < 0:
                raise Exception("Attempted to close a closed struct? %s:%s" % (line_index, line))
            if depth == 1:
                # We reached inside root
                timings.append(current_timing_config)
                current_timing_config = {}
        else:
            raise Exception("Unknown line %s: %s" % (line_index, line))

    return timings


def generate_dt(dev_file, dev_rams_timings, is_mc_section, put_comments, soc):
    dev_file.write("	memory-controller@%s {" % ("7000f000" if is_mc_section else "7000f400"))
    for ram_code, ram_key, ram_timings in dev_rams_timings:
        dev_file.write("\n		emc-timings-%s { \n" % ram_code)
        dev_file.write("			/* %s */\n" % ram_key)
        dev_file.write("			nvidia,ram-code = <%s>;\n" % ram_code)
        for config in ram_timings:
            mem_freq = int(config["SDRAM frequency"]) * 1000
            dev_file.write("\n			timing-%s {\n" % mem_freq)
            dev_file.write("				clock-frequency = <%s>;\n\n" % mem_freq)

            if not is_mc_section:
                added_keys = False
                for key, key_dt in TIMINGS_CFG_KEYS[soc].items():
                    if key_dt in [None, "clock-frequency"]:
                        continue

                    if key_dt in TIMINGS_CFG_KEYS_FLAGS:
                        if key in config:
                            if config[key] == "0x00000001":
                                added_keys = True
                                dev_file.write("				%s;\n" % (key_dt))
                            elif config[key] != "0x00000000":
                                raise Exception("Unknow flag value %s ram %s timing %s key %s for %s" % (config[key], ram_key, mem_freq, key, key_dt))

                    else:
                        if key not in config:
                            raise Exception("Missing ram %s timing %s key %s for %s" % (ram_key, mem_freq, key, key_dt))
                        added_keys = True
                        dev_file.write("				%s = <%s>;\n" % (key_dt, config[key]))
                if added_keys:
                    dev_file.write("\n")

            if put_comments:
                dev_file.write("				nvidia,%s-configuration = <\n" % ("emem" if is_mc_section else "emc"))
                emc_config_keys = MC_CFG_KEYS[soc] if is_mc_section else EMC_CFG_KEYS[soc]
                emc_config_values = config["mc" if is_mc_section else "emc"]
                for key in emc_config_keys:
                    if key not in emc_config_values:
                        raise Exception("Missing ram %s timing %s key %s" % (ram_key, mem_freq, key))
                    dev_file.write("					%s /* %s */\n" % (emc_config_values[key], key))

                dev_file.write("				>;\n")
            else:
                dev_file.write("				nvidia," + ("emem-configuration = <" if is_mc_section else "emc-configuration =  <"))
                emc_config_keys = MC_CFG_KEYS[soc] if is_mc_section else EMC_CFG_KEYS[soc]
                emc_config_values = config["mc" if is_mc_section else "emc"]
                value_col = 2 if is_mc_section else 3
                for key in emc_config_keys:
                    if 3 < value_col:
                        dev_file.write("\n					")
                        value_col = 0
                    value_col += 1
                    if key not in emc_config_values:
                        raise Exception("Missing ram %s timing %s key %s" % (ram_key, mem_freq, key))
                    if 1 < value_col:
                        dev_file.write(" ")
                    dev_file.write(emc_config_values[key])

                dev_file.write(" >;\n")

            dev_file.write("			};\n")
        dev_file.write("		};\n")
    dev_file.write("	};\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates Tegra DT timings')
    parser.add_argument('db', help='.py file name in db folder to load definitions')
    parser.add_argument('--comments', action='store_const', const=True, default=False,
                        help='Generates emem/emc-configuration with comments')

    args = parser.parse_args()

    try:
        db = importlib.import_module("db.%s" % args.db)
    except ModuleNotFoundError:
        print("Database '%s' not found" % args.db)
        exit(1)

    # Give me folder
    try:
        os.makedirs("out")
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    # Inject MC and EMC data into data
    rams_timings = {}
    for ram_soc, soc_rams in db.rams.items():
        rams_timings[ram_soc] = {}
        for ram_key, ram_struct in soc_rams.items():
            print("Parsing SOC:", ram_soc, "RAM:", ram_key)
            rams_timings[ram_soc][ram_key] = parse_ram_struct(ram_struct, ram_soc)

    # Dump into device file
    for dev_key, dev_conf in db.devices.items():
        with open("out/%s.dsi" % dev_key, 'w') as dev_file:
            print("Device:", dev_key)

            dev_soc = dev_conf["soc"]
            dev_rams_timings = []
            for ram_code, ram_key in enumerate(dev_conf["ram"]):
                dev_rams_timings.append((ram_code, ram_key, rams_timings[dev_soc][ram_key]))

            dev_file.write("// SPDX-License-Identifier: GPL-2.0\n\n/ {\n")
            if dev_soc != "T2":
                generate_dt(dev_file, dev_rams_timings, True, args.comments, dev_soc)
                dev_file.write("\n")
            generate_dt(dev_file, dev_rams_timings, False, args.comments, dev_soc)
            dev_file.write("};\n")

